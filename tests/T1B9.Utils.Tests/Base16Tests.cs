using Xunit;

namespace T1B9.Utils.Tests
{
    public class Base16Tests
    {
        [Theory]
        [InlineData("")]
        [InlineData("00")]
        [InlineData("01")]
        [InlineData("000102030405060708090a0b0c0d0e0f")]
        [InlineData("0123456789abcdef")]
        public void RoundTripTest(string data)
        {
            Assert.Equal(data, Base16.Encode(Base16.Decode(data)).ToString());
        }
    }
}
