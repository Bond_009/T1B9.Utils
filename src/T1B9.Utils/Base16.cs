/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace T1B9.Utils
{
    /// <summary>
    /// Converts hexadecimal strings into bytes and bytes into hexadecimal strings.
    /// </summary>
    public static class Base16
    {
        private const string HexCharsLower = "0123456789abcdef";
        private const string HexCharsUpper = "0123456789ABCDEF";

        private const int LastHexSymbol = 0x66; // 102: f

        /// <summary>
        /// Gets a map from an ASCII char to its hex value shifted,
        /// e.g. <c>b</c> -> 11. 0xff means it's not a hex symbol.
        /// </summary>
        internal static ReadOnlySpan<byte> HexLookup => new byte[]
        {
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f
        };

        /// <summary>
        /// Encodes each element of the specified bytes as its hexadecimal string representation.
        /// </summary>
        /// <param name="bytes">An array of bytes.</param>
        /// <param name="lowercase"><c>true</c> to use lowercase hexadecimal characters; otherwise <c>false</c>.</param>
        /// <returns>
        /// A <see cref="ReadOnlySpan{Char}" /> of hexadecimal pairs, where each pair represents the corresponding element in <c>bytes</c>.
        /// </returns>
        public static ReadOnlySpan<char> Encode(ReadOnlySpan<byte> bytes, bool lowercase = true)
        {
            int len = bytes.Length;
            if (len == 0)
            {
                return Array.Empty<char>();
            }

            string hexChars = lowercase ? HexCharsLower : HexCharsUpper;

            // TODO: use string.Create when it's supports spans
            // Ref: https://github.com/dotnet/corefx/issues/29120
            char[] s = new char[len * 2];
            int j = 0;
            for (int i = 0; i < len; i++)
            {
                s[j++] = hexChars[bytes[i] >> 4];
                s[j++] = hexChars[bytes[i] & 0x0f];
            }

            return s;
        }

        /// <summary>
        /// Decodes a hexadecimal string into its binary representation.
        /// </summary>
        /// <param name="str">A sequence of hexadecimal pairs.</param>
        /// <returns>The decoded bytes.</returns>
        public static byte[] Decode(ReadOnlySpan<char> str)
        {
            if (str.Length == 0)
            {
                return Array.Empty<byte>();
            }

            var unHex = HexLookup;

            int byteLen = str.Length / 2;
            byte[] bytes = new byte[byteLen];
            int i = 0;
            for (int j = 0; j < byteLen; j++)
            {
                byte a;
                byte b;
                if (str[i] > LastHexSymbol
                    || (a = unHex[str[i++]]) == 0xff
                    || str[i] > LastHexSymbol
                    || (b = unHex[str[i++]]) == 0xff)
                {
                    ThrowHelper.ThrowArgumentException("String contains non-hex symbols.", nameof(str));
                    break; // Unreachable
                }

                bytes[j] = (byte)((a * 16) | b);
            }

            return bytes;
        }
    }
}
