# T1B9.Utils

A collection of classes that may come in handy when working on C# projects.

## FAQ

### Why isn't this a NuGet package?

Multiple reasons:

- Each utility has it's niche usecase, it's unlikely you would use them all, this way you can include only the ones you really need.
- And I didn't want to create another dependency for my projects.
